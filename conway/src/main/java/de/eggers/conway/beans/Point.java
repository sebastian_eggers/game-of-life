package de.eggers.conway.beans;

/**
 * Bean to hold information of one point of the grid identified by X and Y
 * coordinate.
 * 
 * @author S27587
 */
public class Point {

	// Instance variables
	private int x;
	private int y;

	// Constructor
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	// Getter
	public int getY() {
		return y;
	}

	public int getX() {
		return x;
	}

	// Methods

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Point) {
			Point otherPoint = (Point) obj;
			if (this.x == otherPoint.x && this.y == otherPoint.y) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}

package de.eggers.conway.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.eggers.conway.beans.Cell;
import de.eggers.conway.beans.Point;
import de.eggers.conway.beans.StaticPatternExamples;
import de.eggers.conway.logic.ConvayGameLogic;

/**
 * Spring Controller which exposes two REST APIs in JSON format: <br>
 * 1) /cells-API which calculates next generation based on given cells <br>
 * 2) /cells/examples-API which returns up to 8 different initial example cells
 * 
 * @author Sebastian Eggers
 */
@RestController
@RequestMapping("/api")
public class ConvayGameApi {

	@Autowired
	private ConvayGameLogic gameLogic;

	@RequestMapping(value = "/cells", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cell[][]> cells(@RequestParam(value = "sizeOfGrid", required = true) int sizeOfGrid,
			@RequestParam(value = "activeCells", required = false) String activeCellsFromFrontend) {
		System.out.println("API /cells called with activeCellsFromFrontend:" + activeCellsFromFrontend);

		List<Point> activePoints = parseGivenStringToActivePoints(activeCellsFromFrontend);
		Cell[][] initialCells = gameLogic.createGridOfCellsByGivenActivePoints(sizeOfGrid, activePoints);
		Cell[][] nextGenerationCells = gameLogic.calculateNextGeneration(initialCells);
		return new ResponseEntity<Cell[][]>(nextGenerationCells, HttpStatus.OK);
	}

	@RequestMapping(value = "/cells/examples/{exampleId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cell[][]> examples(@RequestParam(value = "sizeOfGrid", required = true) int sizeOfGrid,
			@PathVariable int exampleId) {
		System.out.println("API /cells/examples/{exampleId} called with exampleId: " + exampleId);

		List<Point> activePoints = StaticPatternExamples.LIST_OF_EXAMPLES.get(exampleId);
		Cell[][] cells = gameLogic.createGridOfCellsByGivenActivePoints(sizeOfGrid, activePoints);
		return new ResponseEntity<Cell[][]>(cells, HttpStatus.OK);
	}

	/**
	 * Parses the given String by frontend to a list of {@link Point}.
	 * 
	 * @param activeCellsFromFrontend
	 * @return List of Points
	 */
	private List<Point> parseGivenStringToActivePoints(String activeCellsFromFrontend) {

		if (activeCellsFromFrontend == null || activeCellsFromFrontend.isEmpty()) {
			return new ArrayList<Point>(); // No active points given

		}

		String[] cellStringArray = activeCellsFromFrontend.split(",");

		List<Point> results = new ArrayList<Point>();
		for (String cell : cellStringArray) {
			String x = cell.substring(0, cell.indexOf("-"));
			String y = cell.substring(cell.indexOf("-") + 1, cell.length());
			results.add(new Point(Integer.valueOf(x), Integer.valueOf(y)));
		}
		return results;
	}

}

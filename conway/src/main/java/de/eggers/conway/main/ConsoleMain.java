package de.eggers.conway.main;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import de.eggers.conway.beans.Cell;
import de.eggers.conway.beans.Point;
import de.eggers.conway.beans.StaticPatternExamples;
import de.eggers.conway.logic.ConvayGameLogic;

/**
 * Main method which uses the console as UI. This class loads the first of many
 * examples provided by {@link StaticPatternExamples} and then populates/prints
 * a new generation at every "enter" hit.
 * 
 * @author Sebastian Eggers
 */
public class ConsoleMain {

	public static void main(String[] args) {
		ConvayGameLogic gameLogic = new ConvayGameLogic();
		int sizeOfGrid = 20;
		List<Point> activePoints = StaticPatternExamples.LIST_OF_EXAMPLES.get(0);
		Cell[][] currentCells = gameLogic.createGridOfCellsByGivenActivePoints(sizeOfGrid, activePoints);
		printCellsOnConsole(currentCells);

		boolean exit = false;
		while (!exit) {
			System.out.println("Enter command ('exit' to quit):");
			String input = new Scanner(System.in).nextLine();
			if (input != null) {
				if ("exit".equals(input)) {
					System.out.println("Exit programm");
					exit = true;
				} else {
					currentCells = gameLogic.calculateNextGeneration(currentCells);
					printCellsOnConsole(currentCells);
				}
			}
		}
	}

	public static void printCellsOnConsole(Cell[][] cellsToPrint) {
		for (int x = 0; x < cellsToPrint.length; x++) {
			System.out.println(Arrays.toString(cellsToPrint[x]));
		}
	}
}
package de.eggers.conway.logic;

import java.util.List;

import org.springframework.stereotype.Component;

import de.eggers.conway.beans.Cell;
import de.eggers.conway.beans.Point;

@Component
public class ConvayGameLogic {

	/**
	 * Create a cells grid array by provided activePoints and given size.
	 * 
	 * @param sizeOfGrid
	 * @param activePoints
	 * @return Cells grid array
	 */
	public Cell[][] createGridOfCellsByGivenActivePoints(int sizeOfGrid, List<Point> activePoints) {
		Cell[][] cells = new Cell[sizeOfGrid][sizeOfGrid];

		for (int x = 0; x < sizeOfGrid; x++) {
			for (int y = 0; y < sizeOfGrid; y++) {
				Point point = new Point(x, y);
				boolean active = false;
				if (activePoints.contains(point)) {
					active = true;
				}
				cells[x][y] = new Cell(new Point(x, y), active);
			}
		}

		return cells;
	}

	/**
	 * Calculates and returns the next generation of cells by the given initial
	 * cells. Initial cells array keeps as it is.
	 * 
	 * @param initialGenerationCells
	 * @return Next generation of cells
	 */
	public Cell[][] calculateNextGeneration(Cell[][] initialGenerationCells) {
		Cell[][] nextGenerationCells = flatCloneOfCells(initialGenerationCells);
		for (int x = 0; x < initialGenerationCells.length; x++) {
			for (int y = 0; y < initialGenerationCells.length; y++) {
				// Check the cell's current state, and count its living
				// neighbors.
				boolean active = nextGenerationCells[x][y].isActive();
				int count = getNumberOfActiveNeighborsByGivenCells(nextGenerationCells[x][y].getPoint(),
						initialGenerationCells);
				boolean nextGenerationActive = false;

				// Calculate next state
				if (active && count < 2) {
					nextGenerationActive = false;
				} else if (active && (count == 2 || count == 3)) {
					nextGenerationActive = true;
				} else if (active && count > 3) {
					nextGenerationActive = false;
				}
				if (!active && count == 3) {
					nextGenerationActive = true;
				}

				nextGenerationCells[x][y].setActive(nextGenerationActive);
			}
		}
		return nextGenerationCells;
	}

	/**
	 * Clones a given cell array by value of not by reference. This is needed to
	 * have the opportunity to always distinguish between current active cells
	 * and next generation cells.
	 * 
	 * @param cells
	 * @return Cells-Array that has identical size and values of provided cells
	 */
	private Cell[][] flatCloneOfCells(Cell[][] cells) {
		Cell[][] resultCells = new Cell[cells.length][cells.length];

		for (int x = 0; x < cells.length; x++) {
			for (int y = 0; y < cells.length; y++) {
				Point point = cells[x][y].getPoint();
				Point copyOfPoint = new Point(point.getX(), point.getY());
				resultCells[x][y] = new Cell(copyOfPoint, cells[x][y].isActive());
			}
		}
		return resultCells;
	}

	/**
	 * Determines the number of active cells for a given point.
	 * 
	 * @param point
	 * @param cells
	 * @return Number of active cells
	 */
	private int getNumberOfActiveNeighborsByGivenCells(Point point, Cell[][] cells) {
		int activeNeighbors = 0; // Counted active neighbor cells to return

		int sizeOfGrid = cells.length;
		int rowAtPoint = point.getX();
		int columnAtPoint = point.getY();
		int columnAtRight;
		int columnAtLeft;
		int rowAtTop;
		int rowAtBottom;

		// Do correction because grid ending regarding Y (resp. column)
		if (point.getY() + 1 == sizeOfGrid) {
			columnAtRight = 0;
			columnAtLeft = point.getY() - 1;
		} else if (point.getY() == 0) {
			columnAtRight = point.getY() + 1;
			columnAtLeft = sizeOfGrid - 1;
		} else {
			columnAtRight = point.getY() + 1;
			columnAtLeft = point.getY() - 1;
		}

		// Do correction because grid ending regarding X (resp. row)
		if (point.getX() + 1 == sizeOfGrid) {
			rowAtBottom = 0;
			rowAtTop = point.getX() - 1;
		} else if (point.getX() == 0) {
			rowAtBottom = point.getX() + 1;
			rowAtTop = sizeOfGrid - 1;
		} else {
			rowAtBottom = point.getX() + 1;
			rowAtTop = point.getX() - 1;
		}

		// 1. Left cell
		if (cells[rowAtPoint][columnAtLeft].isActive()) {
			activeNeighbors++;
		}

		// 2. Top Left cell
		if (cells[rowAtTop][columnAtLeft].isActive()) {
			activeNeighbors++;
		}

		// 3. Top cell
		if (cells[rowAtTop][columnAtPoint].isActive()) {
			activeNeighbors++;
		}

		// 4. Top Right cell
		if (cells[rowAtTop][columnAtRight].isActive()) {
			activeNeighbors++;
		}

		// 5. Right cell
		if (cells[rowAtPoint][columnAtRight].isActive()) {
			activeNeighbors++;
		}

		// 6. Bottom Right cell
		if (cells[rowAtBottom][columnAtRight].isActive()) {
			activeNeighbors++;
		}

		// 7. Bottom cell
		if (cells[rowAtBottom][columnAtPoint].isActive()) {
			activeNeighbors++;
		}

		// 8. Bottom left cell
		if (cells[rowAtBottom][columnAtLeft].isActive()) {
			activeNeighbors++;
		}
		return activeNeighbors;
	}

}

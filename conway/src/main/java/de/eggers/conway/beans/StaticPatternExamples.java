package de.eggers.conway.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains 8 static examples of interesting initial cell patterns.
 * 
 * @author S27587
 */
public class StaticPatternExamples {

	public static List<List<Point>> LIST_OF_EXAMPLES = new ArrayList<List<Point>>();

	private static List<Point> EXAMPLE_PATTERN_1 = new ArrayList<Point>();
	private static List<Point> EXAMPLE_PATTERN_2 = new ArrayList<Point>();
	private static List<Point> EXAMPLE_PATTERN_3 = new ArrayList<Point>();
	private static List<Point> EXAMPLE_PATTERN_4 = new ArrayList<Point>();
	private static List<Point> EXAMPLE_PATTERN_5 = new ArrayList<Point>();
	private static List<Point> EXAMPLE_PATTERN_6 = new ArrayList<Point>();
	private static List<Point> EXAMPLE_PATTERN_7 = new ArrayList<Point>();
	private static List<Point> EXAMPLE_PATTERN_8 = new ArrayList<Point>();

	static {
		// Author's favorite - unknown pattern name
		EXAMPLE_PATTERN_1.add(new Point(6, 8));
		EXAMPLE_PATTERN_1.add(new Point(6, 9));
		EXAMPLE_PATTERN_1.add(new Point(6, 10));
		EXAMPLE_PATTERN_1.add(new Point(7, 8));
		EXAMPLE_PATTERN_1.add(new Point(8, 8));
		EXAMPLE_PATTERN_1.add(new Point(7, 10));
		EXAMPLE_PATTERN_1.add(new Point(8, 10));
		EXAMPLE_PATTERN_1.add(new Point(12, 8));
		EXAMPLE_PATTERN_1.add(new Point(12, 9));
		EXAMPLE_PATTERN_1.add(new Point(12, 10));
		EXAMPLE_PATTERN_1.add(new Point(11, 8));
		EXAMPLE_PATTERN_1.add(new Point(10, 8));
		EXAMPLE_PATTERN_1.add(new Point(11, 10));
		EXAMPLE_PATTERN_1.add(new Point(10, 10));

		// Blinker
		EXAMPLE_PATTERN_2.add(new Point(8, 9));
		EXAMPLE_PATTERN_2.add(new Point(9, 9));
		EXAMPLE_PATTERN_2.add(new Point(10, 9));

		// Beacon
		EXAMPLE_PATTERN_3.add(new Point(8, 8));
		EXAMPLE_PATTERN_3.add(new Point(8, 9));
		EXAMPLE_PATTERN_3.add(new Point(9, 8));
		EXAMPLE_PATTERN_3.add(new Point(11, 11));
		EXAMPLE_PATTERN_3.add(new Point(10, 11));
		EXAMPLE_PATTERN_3.add(new Point(11, 10));

		// Pentadecathlon (-> German: Pulsator) - Parameters of API Call:
		// 1-2,2-3,3-1,3-2,3-35-8,5-9,5-10,6-8,6-10,7-8,7-9,7-10,8-8,8-9,8-10,9-8,9-9,9-10,10-8,10-9,10-10,11-8,11-10,12-8,12-9,12-10
		EXAMPLE_PATTERN_4.add(new Point(5, 8));
		EXAMPLE_PATTERN_4.add(new Point(5, 9));
		EXAMPLE_PATTERN_4.add(new Point(5, 10));
		EXAMPLE_PATTERN_4.add(new Point(6, 8));
		EXAMPLE_PATTERN_4.add(new Point(6, 10));
		EXAMPLE_PATTERN_4.add(new Point(7, 8));
		EXAMPLE_PATTERN_4.add(new Point(7, 9));
		EXAMPLE_PATTERN_4.add(new Point(7, 10));
		EXAMPLE_PATTERN_4.add(new Point(8, 8));
		EXAMPLE_PATTERN_4.add(new Point(8, 9));
		EXAMPLE_PATTERN_4.add(new Point(8, 10));
		EXAMPLE_PATTERN_4.add(new Point(9, 8));
		EXAMPLE_PATTERN_4.add(new Point(9, 9));
		EXAMPLE_PATTERN_4.add(new Point(9, 10));
		EXAMPLE_PATTERN_4.add(new Point(10, 8));
		EXAMPLE_PATTERN_4.add(new Point(10, 9));
		EXAMPLE_PATTERN_4.add(new Point(10, 10));
		EXAMPLE_PATTERN_4.add(new Point(11, 8));
		EXAMPLE_PATTERN_4.add(new Point(11, 10));
		EXAMPLE_PATTERN_4.add(new Point(12, 8));
		EXAMPLE_PATTERN_4.add(new Point(12, 9));
		EXAMPLE_PATTERN_4.add(new Point(12, 10));

		// Glider - Parameters of API Call: 1-2,2-3,3-1,3-2,3-3
		EXAMPLE_PATTERN_5.add(new Point(1, 2));
		EXAMPLE_PATTERN_5.add(new Point(2, 3));
		EXAMPLE_PATTERN_5.add(new Point(3, 1));
		EXAMPLE_PATTERN_5.add(new Point(3, 2));
		EXAMPLE_PATTERN_5.add(new Point(3, 3));

		// Leighweight spaceship - Parameters of API Call:
		// 1-1,1-4,2-5,3-1,3-5,4-2,4-3,4-4,4-5
		EXAMPLE_PATTERN_6.add(new Point(1, 1));
		EXAMPLE_PATTERN_6.add(new Point(1, 4));
		EXAMPLE_PATTERN_6.add(new Point(2, 5));
		EXAMPLE_PATTERN_6.add(new Point(3, 1));
		EXAMPLE_PATTERN_6.add(new Point(3, 5));
		EXAMPLE_PATTERN_6.add(new Point(4, 2));
		EXAMPLE_PATTERN_6.add(new Point(4, 3));
		EXAMPLE_PATTERN_6.add(new Point(4, 4));
		EXAMPLE_PATTERN_6.add(new Point(4, 5));

		// Block
		EXAMPLE_PATTERN_7.add(new Point(1, 1));
		EXAMPLE_PATTERN_7.add(new Point(1, 2));
		EXAMPLE_PATTERN_7.add(new Point(2, 1));
		EXAMPLE_PATTERN_7.add(new Point(2, 2));

		// Boat
		EXAMPLE_PATTERN_8.add(new Point(1, 1));
		EXAMPLE_PATTERN_8.add(new Point(1, 2));
		EXAMPLE_PATTERN_8.add(new Point(2, 1));
		EXAMPLE_PATTERN_8.add(new Point(2, 3));
		EXAMPLE_PATTERN_8.add(new Point(3, 2));

		// Add all examples to list
		LIST_OF_EXAMPLES.add(EXAMPLE_PATTERN_1);
		LIST_OF_EXAMPLES.add(EXAMPLE_PATTERN_2);
		LIST_OF_EXAMPLES.add(EXAMPLE_PATTERN_3);
		LIST_OF_EXAMPLES.add(EXAMPLE_PATTERN_4);
		LIST_OF_EXAMPLES.add(EXAMPLE_PATTERN_5);
		LIST_OF_EXAMPLES.add(EXAMPLE_PATTERN_6);
		LIST_OF_EXAMPLES.add(EXAMPLE_PATTERN_7);
		LIST_OF_EXAMPLES.add(EXAMPLE_PATTERN_8);
	}
}

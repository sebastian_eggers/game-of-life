package de.eggers.conway.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import de.eggers.conway.api.ConvayGameApi;

/**
 * Main class which starts spring boot application. If this startup is used a
 * REST API is exposed by {@link ConvayGameApi}. Other main method which uses
 * console based logic is {@link ConsoleMain}.
 * 
 * @author Sebastian Eggers
 */
@Configuration
@ComponentScan(basePackages = "de.eggers")
@EnableAutoConfiguration
public class SpringBootMain {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMain.class, args);
	}

}

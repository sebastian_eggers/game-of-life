package de.eggers.conway.beans;

public class Cell {

	// Instance variables
	private Point point;
	private boolean active;

	// Constructor
	public Cell(Point point, boolean active) {
		this.point = point;
		this.active = active;
	}

	// Getter and Setter
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Point getPoint() {
		return point;
	}

	// Methods
	@Override
	public String toString() {
		if (isActive()) {
			return "1"; // Used for console printing
		} else {
			return "0"; // Used for console printing
		}
	}

}
